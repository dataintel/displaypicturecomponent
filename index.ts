import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseWidgetDisplayPictureComponent } from './src/base-widget.component';

export * from './src/base-widget.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    BaseWidgetDisplayPictureComponent
  ],
  exports: [
    BaseWidgetDisplayPictureComponent
  ]
})
export class DisplayPictureModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: DisplayPictureModule,
      providers: []
    };
  }
}
