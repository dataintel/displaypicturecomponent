import { Component, OnInit, Output, Input, EventEmitter, ViewContainerRef, ViewEncapsulation } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-displaydicture',
  template: `

  <div class="col-md-4 col-sm-4">
    <div class="card">
        <div class="card-header">
            Featured News
        </div>
        <div class="card-block">
                  <div *ngIf="items?.length > 0" class="news-detail news-detail-wrapper-5835207167829">
                        <div class="news-detail-img col-md-5" [ngStyle]="{'background-image': 'url(' + image +'),url(../assets/img/images.jpg)'}"></div>
                        <h4 class="colored-news-title">
                            <strong class="text-justify" [innerHtml]="title" title="{{title}}"></strong>
                        </h4>
                        <span>
                        <small>
                            <a class="text-muted" [innerHtml]="timestamp"></a>.
                            <span class="text-muted" [innerHtml]="media_displayName">Indo Pos</span>
                        </small>
                        </span>
                        <p class="text-justify" [innerHtml]="text"></p>
                    </div>
                       
                            <!--list iemage yang 5 gambar-->
                            <div class="list-image-news">
                                <div *ngFor="let subitem of items; let i=index" (click)="onSelect(subitem)">
                                    <template [ngIf]="i<12">
                                        <div class="pict" [ngStyle]="{'background-image': 'url(' + subitem.image + '),url(../assets/img/images.jpg)'}"></div>
                                    </template>
                                </div>
                            </div>
                           
                    <div class="text-md-center" *ngIf="items?.length == 0">
                        No Result
                    </div>
           
        </div>

    </div>
</div>

<style>

    /** latest tweet */
.news-detail .news-detail-img {
    float: left;
    width: 30%;
    height: 30%;
    height: 135px;
    margin-right: 10px;
    overflow: hidden;
    background: #ccc no-repeat center center;
    background-size: cover;

}

.list-image-news{
    margin-top: -10px;
    position: relative;
}


#influencer-mention-in-news {
    height: 200px;
    padding-top: 2px;
}

.panel .panel-body {
    padding: 5px 0;
}

.list-image-news .pict {

    float: left;
    margin-right: 5px;
    width: calc(20% - 5px);
    height: 65px;
    background: #eee no-repeat center center;
    background-size: cover;
    border: 1px solid #ccc;
    cursor: pointer;
  
}

.card-header {
    padding: 0.75rem 0.75rem;
    font-weight: bolder;
    color: #ff2e2e;
    margin-bottom: 0;
    background-color: #fefefe;
    border-bottom: 1px solid #cfd8dc;
}

.card-block {
    padding: 0.75rem;
}

.news-detail{
    height: 155px;
    overflow: hidden;
}

.colored-news-title {
    cursor: pointer;
}
.text-justify {
    text-align: justify !important;
}

strong {
    font-weight: bolder;
}

small, .small {
    font-size: 80%;
    font-weight: normal;
}

.text-muted {
    color: #b0bec5 !important;
}


</style>
  
  `,
  styles: []
})

export class BaseWidgetDisplayPictureComponent implements OnInit {
  public image;
  public title;
  public media_displayName;
  public timestamp;
  public text;
  public newsId;

  public items: any;

  constructor() {

  }
  ngOnInit() {
    this.items = [
        {
            "image": "../assets/img/images2.jpg",
            "title":"Djokovic Menangi Laga Perdana",
            "media_displayName":"Indo Pos",
            "timestamp":"3 hours ago",
            "content":"– Juara dua kali Novak Djokovic langsung kerja keras di pertandingan pertamanya. Menghadapi Gilles Simon …",
            "newsId":"1"
        },
        {
            "image": "../assets/img/noimg.png",
            "title":"sdfg",
            "media_displayName":"sadf",
            "timestamp":"12",
            "content":"12",
            "newsId":"2"
        },
        {
            "image": "../assets/img/images3.jpg",
            "title":"Djokovic Menangi Laga Perdana 3",
            "media_displayName":"Indo Pos",
            "timestamp":"3 hours ago",
            "content":"– Juara dua kali Novak Djokovic langsung kerja keras di pertandingan pertamanya. Menghadapi Gilles Simon …",
            "newsId":"1"
        },
        {
            "image": "../assets/img/images.jpg",
            "title":"Djokovic Menangi Laga Perdana21",
            "media_displayName":"Indo Pos",
            "timestamp":"3 hours ago",
            "content":"– Juara dua kali Novak Djokovic langsung kerja keras di pertandingan pertamanya. Menghadapi Gilles Simon …",
            "newsId":"1"
        },
        {
            "image": "../assets/img/images1.jpg",
            "title":"Djokovic Menangi Laga Perdana",
            "media_displayName":"Indo Pos",
            "timestamp":"3 hours ago",
            "content":"– Juara dua kali Novak Djokovic langsung kerja keras di pertandingan pertamanya. Menghadapi Gilles Simon …",
            "newsId":"1"
        },
        
    ];

    this.onSelect(this.items[0]);
    
  }

  //buat load data baru
  ngOnChanges() {
    this.items = [];
  }

  onSelect(subitem) {
    console.log("this.items");
    if (subitem) {
      this.image = subitem.image;
      this.title = subitem.title;
      this.media_displayName = subitem.media_displayName;
      this.timestamp = subitem.timestamp;
      this.text = subitem.content;
      this.newsId = subitem.id;
    }
  }
}
